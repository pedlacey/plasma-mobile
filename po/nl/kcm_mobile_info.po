# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-mobile package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-14 02:17+0000\n"
"PO-Revision-Date: 2023-03-14 13:32+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/ui/main.qml:16
#, kde-format
msgid "System Information"
msgstr "Systeeminformatie"

#: package/contents/ui/main.qml:42
#, kde-format
msgid "Operating System"
msgstr "Besturingssysteem"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Webpage"
msgstr "Webpagina"

#: package/contents/ui/main.qml:70
#, kde-format
msgid "KDE Plasma Version"
msgstr "KDE Plasma-versie"

#: package/contents/ui/main.qml:77
#, kde-format
msgid "KDE Frameworks Version"
msgstr "KDE-Frameworks-versie"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Qt Version"
msgstr "Qt-versie"

#: package/contents/ui/main.qml:91
#, kde-format
msgid "Kernel Version"
msgstr "Kernel-versie"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "OS Type"
msgstr "Type OS"

#: package/contents/ui/main.qml:99
#, kde-format
msgctxt "@label %1 is the CPU bit width (e.g. 32 or 64)"
msgid "%1-bit"
msgstr "%1-bit"

#: package/contents/ui/main.qml:116
#, kde-format
msgid "Processor"
msgid_plural "Processors"
msgstr[0] "Processor"
msgstr[1] "Processors"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Memory"
msgstr "Geheugen"

#: package/contents/ui/main.qml:127
#, kde-format
msgctxt "@label %1 is the formatted amount of system memory (e.g. 7,7 GiB)"
msgid "%1 of RAM"
msgstr "%1 RAM"

#: package/contents/ui/main.qml:129
#, kde-format
msgctxt "Unknown amount of RAM"
msgid "Unknown"
msgstr "Onbekend"

#: package/contents/ui/main.qml:143
#, kde-format
msgid "Copy to clipboard"
msgstr "Naar klembord kopiëren"
