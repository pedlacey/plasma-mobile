# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-mobile package.
# Vincenzo Reale <smart2128vr@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-13 02:17+0000\n"
"PO-Revision-Date: 2022-07-12 14:10+0200\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.3\n"

#: package/contents/ui/FavoritesGrid.qml:224
#: package/contents/ui/FolderGrid.qml:199
#, kde-format
msgid "Remove from favourites"
msgstr "Rimuovi dai preferiti"

#: package/contents/ui/FavoritesGrid.qml:300
#, kde-format
msgid "Add applications to your favourites so they show up here."
msgstr ""
"Aggiungi le applicazioni ai tuoi preferiti in modo che vengano visualizzate "
"qui."

#: package/contents/ui/FolderGrid.qml:204
#, kde-format
msgid "Move out of folder"
msgstr "Sposta fuori dalla cartella"

#: package/contents/ui/GridAppDelegate.qml:65
#, kde-format
msgid "Add to favourites"
msgstr "Aggiungi ai preferiti"

#: package/contents/ui/GridAppList.qml:69
#, kde-format
msgid "Applications"
msgstr "Applicazioni"

#: plugin/pinnedmodel.cpp:150
#, kde-format
msgctxt "Default application folder name."
msgid "Folder"
msgstr "Cartella"
