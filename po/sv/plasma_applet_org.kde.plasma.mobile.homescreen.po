# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-mobile package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-06 02:13+0000\n"
"PO-Revision-Date: 2022-04-08 17:37+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: package/contents/ui/appdrawer/AppDrawerHeader.qml:37
#, kde-format
msgid "Applications"
msgstr "Program"

#: package/contents/ui/private/ConfigOverlay.qml:98
#, kde-format
msgid "Remove"
msgstr "Ta bort"
